package com.waes.diffchecker.util;

import static java.lang.String.*;
import static java.text.MessageFormat.format;

public class FileNameGenerator {

    public static final String RIGHT_PATTERN = "{0}/right";
    public static final String LEFT_PATTERN = "{0}/left";

    public static final String getFileNameFor(long id, String fileNamePattern) {
        return format(fileNamePattern, valueOf(id));
    }

}
