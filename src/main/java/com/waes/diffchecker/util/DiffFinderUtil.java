package com.waes.diffchecker.util;

import com.google.common.collect.Sets;
import com.waes.diffchecker.response.BaseDiffCheckResponse;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Set;

import static com.waes.diffchecker.enums.DiffStatusEnum.DIFFERENT_LENGTH;
import static com.waes.diffchecker.enums.DiffStatusEnum.DIFFERENT_TEXT;
import static com.waes.diffchecker.enums.DiffStatusEnum.SAME_STRUCTURE;
import static com.waes.diffchecker.util.FileNameGenerator.LEFT_PATTERN;
import static com.waes.diffchecker.util.FileNameGenerator.RIGHT_PATTERN;
import static java.lang.String.valueOf;
import static java.nio.channels.FileChannel.MapMode.READ_ONLY;
import static java.text.MessageFormat.format;

public class DiffFinderUtil {

    public static final String READ_MODE = "r";
    public static final long START_POSITION = 0L;

    public static BaseDiffCheckResponse fetchDifferencesForId(long id, String rootFolder) throws IOException {
        File file = new File(rootFolder + format(LEFT_PATTERN, valueOf(id)) );
        File file2 = new File(rootFolder + format(RIGHT_PATTERN, valueOf(id)) );

        return fetchDifferencesForFiles(file, file2);
    }

    public static BaseDiffCheckResponse fetchDifferencesForFiles(File file, File file2) throws IOException {
        FileChannel ch1 = new RandomAccessFile(file, READ_MODE).getChannel();
        FileChannel ch2 = new RandomAccessFile(file2, READ_MODE).getChannel();
        Set<Integer> diffPositions = Sets.newHashSet();

        boolean differentTextLength = ch1.size() != ch2.size();
        if (differentTextLength) {
            return new BaseDiffCheckResponse(DIFFERENT_LENGTH);
        }

        long size = ch1.size();
        ByteBuffer m1 = ch1.map(READ_ONLY, START_POSITION, size);
        ByteBuffer m2 = ch2.map(READ_ONLY, START_POSITION, size);

        for (int pos = 0; pos < size; pos++) {
            if (m1.get(pos) != m2.get(pos)) {
                diffPositions.add(pos);
            }
        }

        if(diffPositions.isEmpty()){
            return new BaseDiffCheckResponse(SAME_STRUCTURE);
        } else {
            return new BaseDiffCheckResponse(DIFFERENT_TEXT, diffPositions);
        }
    }
}
