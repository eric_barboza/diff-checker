package com.waes.diffchecker.exception;

public final class ErrorMessages {

    public static final String ERR_MSG_NULL_DOC = "It is not possible parse a null document.";
}
