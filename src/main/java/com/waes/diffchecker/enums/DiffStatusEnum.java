package com.waes.diffchecker.enums;

public enum DiffStatusEnum {

    DIFFERENT_LENGTH,
    SAME_STRUCTURE,
    DIFFERENT_TEXT,
    ERROR
}
