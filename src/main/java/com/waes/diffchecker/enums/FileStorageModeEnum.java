package com.waes.diffchecker.enums;

public enum  FileStorageModeEnum {

    LOCAL,
    REMOTE;
}
