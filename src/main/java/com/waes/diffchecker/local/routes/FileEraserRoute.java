package com.waes.diffchecker.local.routes;

import com.waes.diffchecker.local.cleaner.service.FileCleanerServiceImpl;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileEraserRoute extends RouteBuilder {

    @Value("${app.storage.cleanperiod}")
    private long cleanperiod;

    @Override
    public void configure() {
        from("timer://foo?fixedRate=true&period=" + cleanperiod)
                .bean(FileCleanerServiceImpl.class, "clean");

    }
}
