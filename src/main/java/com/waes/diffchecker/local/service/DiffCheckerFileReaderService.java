package com.waes.diffchecker.local.service;

import com.waes.diffchecker.response.BaseDiffCheckResponse;

import java.io.IOException;

public interface DiffCheckerFileReaderService {

    BaseDiffCheckResponse read(long id) throws IOException;
}
