package com.waes.diffchecker.local.service;

import com.waes.diffchecker.dto.DiffChunkDTO;
import com.waes.diffchecker.response.FileWriteResponse;

import java.io.IOException;

public interface DiffCheckerFileWriterService {

    public FileWriteResponse write(long id, String fileNamePattern, DiffChunkDTO chunk) throws IOException;
}
