package com.waes.diffchecker.local.service;

import com.waes.diffchecker.dto.DiffChunkDTO;
import com.waes.diffchecker.response.BaseDiffCheckResponse;
import com.waes.diffchecker.response.FileWriteResponse;
import com.waes.diffchecker.util.DiffFinderUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.logging.Logger;

import static com.waes.diffchecker.enums.DiffStatusEnum.ERROR;
import static com.waes.diffchecker.enums.FileWriteResponseStatusEnum.FAILED;
import static com.waes.diffchecker.enums.FileWriteResponseStatusEnum.SUCCESS;

@Service
public class DiffCheckerServiceImpl implements DiffCheckerService {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Value("${app.storage.rootfolder}")
    private String rootFolder;

    @Override
    public BaseDiffCheckResponse checkDiffFor(long id)  {
        try {
            return DiffFinderUtil.fetchDifferencesForId(id, rootFolder);
        } catch (FileNotFoundException e) {
            log.severe("Could not read file for id: " + id);
            log.severe(e.getMessage());
            return new BaseDiffCheckResponse(ERROR, "Please make sure you have have inserted both left and right files before checking differences.");
        } catch (IOException e) {
            log.severe("Could not fetch differences for id: " + id);
            log.severe(e.getMessage());
            return new BaseDiffCheckResponse(ERROR);
        }
    }

    @Override
    public FileWriteResponse write(String fileName, DiffChunkDTO chunk)  {
        try {
            FileUtils.write(new File(rootFolder + fileName), chunk.getData());
            return new FileWriteResponse(SUCCESS);
        } catch (IOException e) {
            log.severe("Error while writing file: " + fileName);
            log.severe(e.getMessage());
            return new FileWriteResponse(FAILED);
        }
    }
}
