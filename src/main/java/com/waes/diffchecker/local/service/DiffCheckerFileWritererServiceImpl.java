package com.waes.diffchecker.local.service;

import com.waes.diffchecker.dto.DiffChunkDTO;
import com.waes.diffchecker.enums.FileStorageModeEnum;
import com.waes.diffchecker.remote.service.DiffRemoteCheckerService;
import com.waes.diffchecker.response.FileWriteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static com.waes.diffchecker.enums.FileStorageModeEnum.LOCAL;
import static com.waes.diffchecker.enums.FileStorageModeEnum.REMOTE;
import static com.waes.diffchecker.enums.FileStorageModeEnum.valueOf;
import static com.waes.diffchecker.util.FileNameGenerator.*;

@Service
public class DiffCheckerFileWritererServiceImpl implements DiffCheckerFileWriterService {

    private DiffCheckerService diffCheckerService;
    private DiffRemoteCheckerService remoteCheckerService;

    @Value("${app.storage.mode}")
    private String fileStorageMode;

    @Autowired
    public DiffCheckerFileWritererServiceImpl(DiffCheckerService diffCheckerService, DiffRemoteCheckerService remoteCheckerService) {
        this.diffCheckerService = diffCheckerService;
        this.remoteCheckerService = remoteCheckerService;
    }

    @Override
    public FileWriteResponse write(long id, String fileNamePattern, DiffChunkDTO chunk)  {
        FileStorageModeEnum storageMode = valueOf(fileStorageMode);

        String fileName = getFileNameFor(id, fileNamePattern);

        if(LOCAL.equals(storageMode)){
            return diffCheckerService.write(fileName, chunk);
        } else if (REMOTE.equals(storageMode)){
            return remoteCheckerService.write(fileName, chunk);
        }

        throw new IllegalStateException("Could not diff files from invalid storage mode.");
    }
}
