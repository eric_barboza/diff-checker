package com.waes.diffchecker.local.service;

import com.waes.diffchecker.enums.FileStorageModeEnum;
import com.waes.diffchecker.remote.service.DiffRemoteCheckerService;
import com.waes.diffchecker.response.BaseDiffCheckResponse;
import com.waes.diffchecker.util.FileNameGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static com.waes.diffchecker.enums.FileStorageModeEnum.*;

@Service
public class DiffCheckerFileReaderServiceImpl implements DiffCheckerFileReaderService {

    private DiffCheckerService diffCheckerService;
    private DiffRemoteCheckerService remoteCheckerService;

    @Value("${app.storage.mode}")
    private String fileStorageMode;

    @Autowired
    public DiffCheckerFileReaderServiceImpl(DiffCheckerService diffCheckerService, DiffRemoteCheckerService remoteCheckerService) {
        this.diffCheckerService = diffCheckerService;
        this.remoteCheckerService = remoteCheckerService;
    }

    @Override
    public BaseDiffCheckResponse read(long id)  {
        FileStorageModeEnum storageMode = valueOf(fileStorageMode);
        if(LOCAL.equals(storageMode)){
            return diffCheckerService.checkDiffFor(id);
        } else if (REMOTE.equals(storageMode)){
            return remoteCheckerService.checkDiffFor(id);
        }

        throw new IllegalStateException("Could not diff files from invalid storage mode.");
    }
}
