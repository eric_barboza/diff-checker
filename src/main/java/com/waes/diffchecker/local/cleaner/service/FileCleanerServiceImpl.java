package com.waes.diffchecker.local.cleaner.service;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Date;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static java.lang.String.valueOf;

@Service
public class FileCleanerServiceImpl implements FileCleanerService {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Value("${app.storage.rootfolder}")
    private String rootFolder;

    public static final int ONE_HOUR = 1 * 60 * 60 * 1000;
    @Override
    public void clean() {
        log.info("Init old files cleaner");
        File directory = new File(rootFolder);

        File[] files = directory.listFiles();

        if(ArrayUtils.isEmpty(files)) {
            return;
        }

        Stream.of(files)
                .filter(f -> isFileExpired(f))
                .forEach(f -> {
                    log.info("Removing file:" + f.getName());
                    f.delete();
                });
        log.info("Ending old files cleaner");
    }

    private boolean isFileExpired(File f) {
        return ONE_HOUR < (new Date().getTime() - f.lastModified());
    }
}
