//package com.waes.diffchecker.config;
//
//import com.google.common.base.MoreObjects;
//import com.google.common.collect.Lists;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//
//@Configuration
//@EnableWebSecurity
//public class SecurityConfig extends WebSecurityConfigurerAdapter{
//
//    public static final String FAKE_PWD = "abc";
//    public static final String FAKE_USER = "tester";
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .csrf().disable()
//                .authorizeRequests()
//                .antMatchers("/**").authenticated()
//                .and()
//                .authenticationProvider(fakeAuth())
//                .httpBasic();
//    }
//
//    @Override
//    public void configure(AuthenticationManagerBuilder auth) throws Exception{
//        auth.inMemoryAuthentication()
//                .withUser(FAKE_USER).password(FAKE_PWD).roles("USER", "ADMIN");
//    }
//
//    private AuthenticationProvider fakeAuth() {
//        return new AuthenticationProvider() {
//            @Override
//            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//
//                boolean validUserPwd = MoreObjects.firstNonNull(authentication.getPrincipal(), authentication.getCredentials()) != null;
//                boolean validCredentials = validUserPwd && FAKE_PWD.equals(authentication.getCredentials()) && FAKE_USER.equals(authentication.getPrincipal());
//                if(validCredentials) {
//                    return new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(), Lists.newArrayList(new SimpleGrantedAuthority("USER")));
//                }
//
//                throw new BadCredentialsException("invalid Credentials");
//            }
//
//            @Override
//            public boolean supports(Class<?> aClass) {
//                return true;
//            }
//        };
//    }
//}
