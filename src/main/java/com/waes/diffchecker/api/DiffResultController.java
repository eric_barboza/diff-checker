package com.waes.diffchecker.api;

import com.waes.diffchecker.local.service.DiffCheckerFileReaderService;
import com.waes.diffchecker.response.BaseDiffCheckResponse;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping( "/v1/diff/{id}")
public class DiffResultController {

    private DiffCheckerFileReaderService diffChecker;
    public DiffResultController(DiffCheckerFileReaderService diffChecker) {
        this.diffChecker = diffChecker;
    }

    @RequestMapping(method = POST)
    public BaseDiffCheckResponse findDiffs(@PathVariable long id) throws IOException {
        return diffChecker.read(id);
    }
}
