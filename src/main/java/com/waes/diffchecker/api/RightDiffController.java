package com.waes.diffchecker.api;

import com.waes.diffchecker.dto.DiffChunkDTO;
import com.waes.diffchecker.local.service.DiffCheckerFileWriterService;
import com.waes.diffchecker.response.FileWriteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.IOException;

import static com.waes.diffchecker.util.FileNameGenerator.RIGHT_PATTERN;
import static org.apache.commons.io.FileUtils.write;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping( "/v1/diff/{id}/right")
public class RightDiffController {

    private DiffCheckerFileWriterService fileWriterService;

    @Autowired
    public RightDiffController(DiffCheckerFileWriterService fileWriterService) {
        this.fileWriterService = fileWriterService;
    }

    @RequestMapping(method = POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public FileWriteResponse receiveRightChunk(@Valid
                                     // Regular expression for validating Base64 encoded text.
                                     @Pattern(message = "Not Base64 encoded", regexp = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$")
                                     @RequestBody DiffChunkDTO rightChunk,
                                     @PathVariable long id) throws IOException {

        return fileWriterService.write(id, RIGHT_PATTERN, rightChunk);
    }
}
