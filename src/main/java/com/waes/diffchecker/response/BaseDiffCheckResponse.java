package com.waes.diffchecker.response;

import com.google.common.collect.Sets;
import com.waes.diffchecker.enums.DiffStatusEnum;

import java.util.HashSet;
import java.util.Set;

public class BaseDiffCheckResponse {
    private DiffStatusEnum status;
    private Set<Integer> positions = new HashSet<>();
    private String message;

    public BaseDiffCheckResponse(DiffStatusEnum status) {
        this.status = status;
    }

    public BaseDiffCheckResponse(DiffStatusEnum status, Set<Integer> positions) {
        this(status);
        this.positions = positions;
    }

    public BaseDiffCheckResponse(DiffStatusEnum status, String message) {
        this(status);
        this.message = message;
    }

    public DiffStatusEnum getStatus() {
        return status;
    }

    public Set<Integer> getPositions() {
        return positions;
    }

    public String getMessage() {
        return message;
    }
}
