package com.waes.diffchecker.response;

import com.waes.diffchecker.enums.DiffStatusEnum;

import java.util.Set;

public class DifferenceDiffCheckResponse extends BaseDiffCheckResponse{
    private Set<Integer> positions;


    public DifferenceDiffCheckResponse(DiffStatusEnum status, Set<Integer> positions) {
        super(status);
        this.positions = positions;
    }

    public Set<Integer> getPositions() {
        return positions;
    }
}
