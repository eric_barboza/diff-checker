package com.waes.diffchecker.response;


import com.waes.diffchecker.enums.FileWriteResponseStatusEnum;

public class FileWriteResponse {
    FileWriteResponseStatusEnum status;

    public FileWriteResponse(FileWriteResponseStatusEnum status) {
        this.status = status;
    }

    public FileWriteResponseStatusEnum getStatus() {
        return status;
    }
}
