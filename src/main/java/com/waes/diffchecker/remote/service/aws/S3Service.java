package com.waes.diffchecker.remote.service.aws;

import com.amazonaws.services.s3.model.S3Object;

import java.io.IOException;

public interface S3Service {

    S3Object fetchObject(String region, String bucketName, String fileName);

    String writeObject(String clientRegion, String bucketName, long expireTime, String fileName, String data) throws IOException;
}
