package com.waes.diffchecker.remote.service.aws;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

@Service
public class S3ServiceImpl implements S3Service {

    @Override
    public S3Object fetchObject(String region, String bucketName, String fileName) {
        return getclient(region)
                .getObject(bucketName, fileName);
    }

    @Override
    public String writeObject(String clientRegion, String bucketName,long expireTime, String fileName, String data) throws IOException {
        URL url = preSignURL(clientRegion, bucketName, expireTime, fileName);

        int responseCode = sendRequest(url, data);

        System.out.println("HTTP response code: " + responseCode);
        return url.toString();
    }

    private int sendRequest(URL url, String data) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("PUT");
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(data);
        out.close();
        return connection.getResponseCode();
    }

    private URL preSignURL(String clientRegion,String bucketName, long expireTime, String fileName) {
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName)
                .withMethod(HttpMethod.PUT)
                .withExpiration(getExpirationDate(expireTime));
        return getclient(clientRegion).generatePresignedUrl(generatePresignedUrlRequest);
    }

    private Date getExpirationDate(long expireTime) {
        Date expiration = new Date();
        expiration.setTime(expiration.getTime() + expireTime);
        return expiration;
    }

    private AmazonS3 getclient(String clientRegion) {
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new ProfileCredentialsProvider())
                .withRegion(clientRegion)
                .build();
    }
}
