package com.waes.diffchecker.remote.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.waes.diffchecker.dto.DiffChunkDTO;
import com.waes.diffchecker.enums.DiffStatusEnum;
import com.waes.diffchecker.remote.service.aws.S3Service;
import com.waes.diffchecker.response.BaseDiffCheckResponse;
import com.waes.diffchecker.response.FileWriteResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import static com.waes.diffchecker.enums.DiffStatusEnum.ERROR;
import static com.waes.diffchecker.enums.FileWriteResponseStatusEnum.FAILED;
import static com.waes.diffchecker.enums.FileWriteResponseStatusEnum.SUCCESS;
import static com.waes.diffchecker.util.DiffFinderUtil.*;
import static com.waes.diffchecker.util.FileNameGenerator.*;
import static com.waes.diffchecker.util.FileNameGenerator.RIGHT_PATTERN;
import static java.lang.String.valueOf;
import static org.apache.commons.io.FileUtils.copyInputStreamToFile;

@Service
public class DiffRemoteCheckerServiceImpl implements DiffRemoteCheckerService {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Value("${aws.s3.bucket}")
    private String bucketName;

    @Value("${aws.s3.region}")
    private String clientRegion;

    @Value("${aws.s3.expiretime}")
    private long expireTime;

    @Value("${app.storage.rootfolder}")
    private String rootFolder;

    private S3Service s3Service;
    public DiffRemoteCheckerServiceImpl(S3Service s3Service) {
        this.s3Service = s3Service;
    }

    @Override
    public BaseDiffCheckResponse checkDiffFor(long id)  {
        String leftFileName = getFileNameFor(id, LEFT_PATTERN);
        String rightFileName = getFileNameFor(id, RIGHT_PATTERN);

        S3Object leftObject = s3Service.fetchObject(clientRegion, bucketName, leftFileName);
        S3Object rightObject = s3Service.fetchObject(clientRegion, bucketName, rightFileName);

        S3ObjectInputStream leftStream = leftObject.getObjectContent();
        S3ObjectInputStream rightStream = rightObject.getObjectContent();

        try {
            copyInputStreamToFile(leftStream, new File(rootFolder + getFileNameFor(id, LEFT_PATTERN)));
            copyInputStreamToFile(rightStream, new File(rootFolder +  getFileNameFor(id, RIGHT_PATTERN)));

            return fetchDifferencesForId(id,rootFolder);
        } catch (IOException e) {
            log.severe("Could not fetch differences for id: " + id);
            log.severe(e.getMessage());
            return new BaseDiffCheckResponse(ERROR);
        }
    }

    @Override
    public FileWriteResponse write(String fileName, DiffChunkDTO chunk) {
        try {
            s3Service.writeObject(clientRegion, bucketName,expireTime, fileName, chunk.getData());
            return new FileWriteResponse(SUCCESS);
        } catch(AmazonServiceException e) {
            log.severe("Amazon S3 couldn't process request.");
            log.severe(e.getMessage());
        } catch(SdkClientException e) {
            log.severe("Amazon S3 couldn't be contacted for a response, or the client couldn't parse the response from Amazon S3.");
            log.severe(e.getMessage());
        } catch (IOException e) {
            log.severe("Error while writing S3 object.");
            log.severe(e.getMessage());
        }

        return new FileWriteResponse(FAILED);
    }
}
