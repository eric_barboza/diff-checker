package com.waes.diffchecker.remote.service;

import com.waes.diffchecker.dto.DiffChunkDTO;
import com.waes.diffchecker.response.BaseDiffCheckResponse;
import com.waes.diffchecker.response.FileWriteResponse;

import java.io.IOException;

public interface DiffRemoteCheckerService {

    BaseDiffCheckResponse checkDiffFor(long id);

    FileWriteResponse write(String fileName, DiffChunkDTO data);
}
