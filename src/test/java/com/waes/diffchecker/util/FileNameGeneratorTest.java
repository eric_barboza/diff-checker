package com.waes.diffchecker.util;

import org.junit.Assert;
import org.junit.Test;

import java.text.MessageFormat;

import static com.waes.diffchecker.util.FileNameGenerator.*;
import static com.waes.diffchecker.util.FileNameGenerator.LEFT_PATTERN;
import static com.waes.diffchecker.util.FileNameGenerator.RIGHT_PATTERN;
import static java.lang.String.valueOf;
import static java.text.MessageFormat.*;
import static java.text.MessageFormat.format;
import static org.junit.Assert.assertEquals;

public class FileNameGeneratorTest {

    @Test
    public void testValidLeftFileName() {
        long fakeId = 111;
        String leftFileName = getFileNameFor(fakeId, LEFT_PATTERN);

        assertEquals(leftFileName, format(LEFT_PATTERN, fakeId));
    }

    @Test
    public void testValidRightFileName() {
        long fakeId = 111;
        String rightFileName = getFileNameFor(fakeId, RIGHT_PATTERN);

        assertEquals(rightFileName, format(RIGHT_PATTERN, fakeId));
    }

}
