package com.waes.diffchecker.util;

import com.waes.diffchecker.response.BaseDiffCheckResponse;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static com.waes.diffchecker.enums.DiffStatusEnum.DIFFERENT_LENGTH;
import static com.waes.diffchecker.enums.DiffStatusEnum.DIFFERENT_TEXT;
import static com.waes.diffchecker.enums.DiffStatusEnum.SAME_STRUCTURE;
import static com.waes.diffchecker.util.DiffFinderUtil.*;
import static org.junit.Assert.assertEquals;

public class DiffFinderUtilTest {

    @Test
    public void testValidFilesShouldReturnSuccessResponseStatus() throws IOException {
        File leftFile = getContentFrom("111_left_sameSize_sameContent");
        File rightFile = getContentFrom("111_right_sameSize_sameContent");

        BaseDiffCheckResponse diffResponse = fetchDifferencesForFiles(leftFile, rightFile);

        assertEquals(SAME_STRUCTURE, diffResponse.getStatus());
    }

    @Test
    public void testDifferentFileSizesShouldReturnFailuresResponseStatus() throws IOException {
        File leftFile = getContentFrom("222_left_diffSize");
        File rightFile = getContentFrom("222_right_diffSize");

        BaseDiffCheckResponse diffResponse = fetchDifferencesForFiles(leftFile, rightFile);

        assertEquals(DIFFERENT_LENGTH, diffResponse.getStatus());
    }

    @Test
    public void testSameFileSizesWithDiffContentShouldReturnDiffPositions() throws IOException {
        File leftFile = getContentFrom("333_left_sameSize_diffContent");
        File rightFile = getContentFrom("333_right_sameSize_diffContent");

        BaseDiffCheckResponse diffResponse = fetchDifferencesForFiles(leftFile, rightFile);

        assertEquals(DIFFERENT_TEXT, diffResponse.getStatus());
        assertEquals(6, diffResponse.getPositions().size());
    }


    private File getContentFrom(String filePath) {
        ClassLoader classLoader = this.getClass().getClassLoader();
        return new File(classLoader.getResource(filePath).getFile());
    }
}
