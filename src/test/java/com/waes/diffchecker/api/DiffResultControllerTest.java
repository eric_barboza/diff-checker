package com.waes.diffchecker.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waes.diffchecker.ApplicationStarter;
import com.waes.diffchecker.dto.DiffChunkDTO;
import com.waes.diffchecker.local.service.DiffCheckerFileReaderService;
import com.waes.diffchecker.response.BaseDiffCheckResponse;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static com.google.common.base.CharMatcher.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.hamcrest.Matchers;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ApplicationStarter.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class DiffResultControllerTest {

    @Autowired
    private MockMvc mvc;

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );

    @Test
    public void testPostValidChunk() throws Exception {
        DiffChunkDTO diffChunk = new DiffChunkDTO();
        diffChunk.setData("aaaacbbbbb");

        mvc.perform(post("/v1/diff/111/left")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(diffChunk)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(status().isOk());

        File file = getContentFrom("tmp_files/111/left");
        assertTrue(file.exists());
    }

    @Test
    public void testPostValidRightChunk() throws Exception {
        DiffChunkDTO diffChunk = new DiffChunkDTO();
        diffChunk.setData("aaaacbbbbb");

        mvc.perform(post("/v1/diff/111/right")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertObjectToJsonBytes(diffChunk)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(status().isOk());

        File file = getContentFrom("tmp_files/111/right");
        assertTrue(file.exists());

    }

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    private File getContentFrom(String filePath) {
        return new File("tmp_files/111/right");
    }
}
